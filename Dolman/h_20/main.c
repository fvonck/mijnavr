#include <avr/io.h>
#include <util/delay.h>

#define BAUD   38400   // F_CPU is 8000000 Hz
#define UBRR_VALUE ( ((F_CPU) + 8UL*(BAUD)) / (16UL*(BAUD)) - 1UL )

void uart_init(unsigned int ubrr)
{  UBRR0L = ubrr;
   UBRR0H = (ubrr >> 8);
   UCSR0C = _BV(URSE0L)|_BV(UCSZ01)|_BV(UCSZ00);
   UCSR0B = _BV(TXEN0);
}

int main(void)
{
  int i;

  uart_init(UBRR_VALUE);

  while (1) {
      for (i=0; i<=9; i++) {
      while ( !(UCSR0A & _BV(UDR0E)) ) {};
      UDR = i + '0';
          }
      _delay_ms(500);
        }
}
