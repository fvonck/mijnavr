#define F_CPU 16000000UL 
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define BAUD 9600UL
#define UBRR0_VALUE ((F_CPU/(16UL*BAUD)) - 1UL)

#define begin {
#define end }	

// hallo 
int main(void)
begin
  UBRR0H = (UBRR0_VALUE >> 8);
  UBRR0L = UBRR0_VALUE;
  UCSR0B = (1<<TXEN0) | (1<<RXEN0);
  UCSR0C = (1<<UCSZ00) | (1<<UCSZ01); //8bit
  DDRB |= 0x0f |(1<<PB0);
  PORTB = 0x00; 
  while(1)
  begin
    UDR0 = 'h';
    UDR0 = 'a';
    _delay_ms(200);
    PORTB |= (1 <<PB0); 
    _delay_ms(200);	
    PORTB &= ~(1 <<PB0);	
  end //while 
end // main

