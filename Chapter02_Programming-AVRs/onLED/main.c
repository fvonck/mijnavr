#include <avr/io.h>
#include <util/delay.h>
#define begin {
#define end }	

int main(void) 
begin
  DDRB |=  0b00001111;;
  while(1)
  begin
     _delay_ms(1000);  
     PORTB = 0b00001111;
     _delay_ms(1000);
     PORTB = 0b00000000;
     _delay_ms(1000);
  end //while
  return 0;  
end //main	

