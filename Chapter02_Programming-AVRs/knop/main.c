/* Blinker Demo */

// ------- Preamble -------- //
#include <avr/io.h>                        /* Defines pins, ports, etc */
#include <util/delay.h>                     /* Functions to waste time */
#include "USART.h"
#define begin {
#define end }



int main(void)

	
begin 
  
 initUSART();
int i = 0;
  // -------- Inits --------- //
  //Maak van 1e  pin van PORTB output
  DDRB |= (1 << PB0) ;            /* Data Direction Register B: */
 
  PORTB &=~(1 << PB0);
  //Maak van 3 e pin van PORTD een input //
  DDRC  |=  (1 << PC0) | (1 << PC1);  
  PORTC |= (1 << PC0) | (1 << PC1);
  // eventloop; zet led op pin PB0 aan  als schakelaar op PD2 is ingedrukt //
  while (1) 
  begin 
    if((PINC & ((1 << PC0) | (1 << PC1))) == 0)
    begin
      PORTB |= (1 << PB0);
    end
    else
    begin
       PORTB &= ~(1 << PB0);
    end   
  end //while
  return 0;                            /* This line is never reached */
end //main
